#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

int port;
int conn;
int socket_client;
struct addrinfo hints;
struct addrinfo *result;
struct addrinfo *rp;
char message[80];
int s;
int wr;
int rd;
int nfds=1;
char buffer[100];
char nick[]="/nick";
char * user;
int i=0;
int k=0;
int j=0;


int get_addr_info(char*nom , char* port){
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
        hints.ai_flags = 0;
        hints.ai_protocol = 0;
        result=malloc(sizeof(struct addrinfo));
        s=getaddrinfo(nom,port,&hints,&result);
        if (s != 0) {
               fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
               exit(EXIT_FAILURE);
           }
        else{
            printf("initialization of serv addr : result initialised\n");
           }
         return s;
}
int do_socket(int domain, int type, int protocol){
    int a;
    a=socket(domain,type,protocol);
    if(a<0){
    perror("socket");
    }
    return a;
}

int main(int argc,char** argv)
{

    if (argc != 3)
    {
        fprintf(stderr,"usage: CLIENT hostname port\n");
        return 1;
    }
    get_addr_info(argv[1],argv[2]);

//get the socket
//s = do_socket()
//connect to remote socket
//do_connect()

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        socket_client = do_socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (connect(socket_client, rp->ai_addr, rp->ai_addrlen) != -1){
            break;                  /* Success */
        }
        else{
            printf("Errer de connexion de type : \n");
            perror("connect");
        }
    }

    if (rp == NULL) {               /* No address succeeded */
        fprintf(stderr, "Could not connect\n");
        exit(EXIT_FAILURE);
    }


//get user input
//readline()
for(;;){
   printf("Entrez votre message: ");
    fgets(message,255,stdin);

//send message to the server
//handle_client_message()

    if (strcmp(message,"quit\n")==0){
        wr = write(socket_client,message,80);
        if(wr<0){
            perror("write\n");
        }
        printf("end\n");
        exit(33);
    }
    else{
        wr = write(socket_client,message,80);
        if(wr<0){
            perror("write\n");
        }
        memset(message, 80, '\0');
        rd = read(socket_client,message,80);
        if(strcmp(message,"end")==0){
             printf("max of clients reached\n");
             exit(33);
        }
        printf("[server]:%s\n",message);
    }
    
    
    }
}