#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <time.h>
#include <arpa/inet.h>

#define POLL_SIZE 32
#define MAX_SIZE 20
struct sockaddr_in adresse;
struct pollfd poll_set[POLL_SIZE];
int socket_server;
int bind_server;
int adrlen;
char message[80];
int accepted_socket;
int sock_serv;
int rd;
int wr;
int timeout;
int nfds=1;
int polling;
int new_socket;
int indice_client;
int a;
int c;
int i=0;
int j=0;
int k=0;
int r=0;
int h=0;
int d=0;
int e=0;
struct tm sTm;
int size;
char *t;
char *nickk;
char *command;
char nickname[]="/nick";
char who[]="/who";
char whois[]="/whois";
char welcome[50]="welcome on the chat ";
char welcome1[50]="welcome on the chat ";
struct info_client {
    int socket;
    char nick[30];
    char ip[20];
    char date[30];
    int port;
    int donenick;
};
struct info_clients{
    struct info_client client[MAX_SIZE];
    int nb_clients;
};
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
int do_socket(){
    socket_server=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    return socket_server;
}
int do_bind(char* port){
    memset(&adresse,0,sizeof(adresse));
    adresse.sin_addr.s_addr=htonl(INADDR_ANY);
    adresse.sin_family=AF_INET;
    adresse.sin_port=htons(atoi(port));
    a=bind(socket_server,(struct sockaddr*)&adresse,sizeof(struct sockaddr));
    if(a==-1){
        perror("bind");
    }
    return a;
}
void nick_name(struct info_clients clients,int r){
    j=0;
    t=malloc(sizeof(char)*30);
    while(message[r+2]!='\0'){
        t[j]=message[r+1];
        r++;
        j++;
    }
    t[j]='\0';
}
void nick_who(struct info_clients clients,int d){
    j=0;
    while(message[d+2]!='\0'){
        // on parcours jusqu'à la fin et on le met dans
        nickk[j]=message[d+1];
        d++;
        j++;
    }
    nickk[j]='\0';
}
int which_command(int i,char* message,struct info_clients clients){
    c=0;
    r=0;
    // ici je détecte un espace dans le message pour déterminer le type de commande 
    while(message[r]!=' ')
        {
            command[r]=message[r];
            r++;
        }
    d=r;
    e=d;
    command[r]='\0';
    // voir si c'est la commande /nick 
    if(strcmp(command,nickname)==0){
        // on met le nickname dans la structure avec cette fonction ci dessous
        nick_name(clients,r);
        return 3;
    }
    // voir si c'est la commande whois
    else if(strcmp(command,whois)==0){
        nick_who(clients,d);
        d=e;
        return 2;
    }
    else{
        return 0;
    }
}
int do_listen(){
    int lis;
    lis=listen(socket_server,20);
    if (lis<0){
        perror("listen");
    }
    return lis;
}
int do_accept(){
    adrlen=sizeof(adresse);
    accepted_socket=accept(socket_server,(struct sockaddr*)&adresse,(socklen_t*)&adrlen);
    if(accepted_socket==-1){
        perror("accept\n");
     }
    return accepted_socket;
}
int do_read(char* message,int accepted_socket){
    rd=read(accepted_socket,message,80);
    if(rd<0){
        perror("read");
    }
}
int do_write(char *message,int accepted_socket){
wr=write(accepted_socket,message,80);
    if(wr<0){
        perror("write");
    }
}


int main(int argc, char** argv)
{
    command=malloc(30*sizeof(char));
    nickk=malloc(sizeof(char)*30);
    struct info_clients clients;
    char tim[30];
    char ip[30];
    if (argc != 2)
    {
        fprintf(stderr, "usage: RE216_SERVER port\n");
        return 1;
    }
    //create the socket, check for validity!
    //do_socket()
    sock_serv=do_socket();
    //init the serv_add structure
    //init_serv_addr()
    //perform the binding
    //we bind on the tcp port specified
    do_bind(argv[1]);
    //specify the socket to be a server socket and listen for at most 20 concurrent client
    //listen()
    do_listen();
    //accept connection from client
    //do_accept()
    memset(poll_set,0,sizeof(poll_set));
    poll_set[0].fd=sock_serv;
    poll_set[0].events=POLLIN;
    timeout = (3 * 60 * 1000);
    //do_accept();
    memset(&clients,0,sizeof(clients));
    for( i = 0; i < MAX_SIZE; i++)
    {
        memset(&clients.client[i],0,sizeof(clients.client[i]));
    }
    
    clients.nb_clients=0;
    for(i=0;i<MAX_SIZE;i++){
        clients.client[i].donenick=0;
    }
    for (;;)
    {
        polling=poll(poll_set,nfds,timeout);
        if (polling<0){
            perror("poll\n");
            break;
        }
        if(polling==0){
            printf("  poll() timed out.  End program.\n");
            break;
        }
        for (i = 0; i < nfds; i++){
            if(poll_set[i].revents == POLLIN){
                //cas s'une nouvelle connecion
                if(poll_set[i].fd==sock_serv){
                    new_socket=do_accept();
                    //on la met en socket de poll_set
                    poll_set[nfds].fd = new_socket;
                    poll_set[nfds].events = POLLIN;
                    nfds++;
                    clients.nb_clients++;       
                }
                else{
                    // le cas d'une ancienne connexion dont le numéro de socket est déja sauvegardé
                    if(nfds>MAX_SIZE){
                        do_write("end",poll_set[i].fd);
                    }
                    if(poll_set[i].fd>0){
                        strcpy(ip,inet_ntoa(adresse.sin_addr));
                        time_t now = time (0);
                        gmtime_r (&now, &sTm);
                        strftime (tim,sizeof(tim),"%Y-%m-%d %H:%M:%S", &sTm);
                        clients.client[i].socket=new_socket;
                        strcpy(clients.client[i].ip,ip);
                        strcpy(clients.client[i].date,tim);
                        rd=do_read(message,poll_set[i].fd);
                        c=which_command(i,message,clients);
                        // le cas d'une wommande /nick
                        if(c==3){
                            memset(message,'\0',80);
                            printf("%s\n",t);
                            strcpy(clients.client[i].nick,t);
                            memset(message,'\0',30);
                            clients.client[i].donenick=1;
                            strcat(welcome,clients.client[i].nick);
                            strcpy(message,welcome);
                            do_write(message,poll_set[i].fd);
                            memset(message,'\0',80);
                            memset(welcome,'\0',50);
                            strcpy(welcome,welcome1);
                        }
                        // cas d'une commande /whois
                        if(c==2){      
                            for( j = 0; j < MAX_SIZE; j++)
                            {  
                                //On recherche le nom dans la liste des nicknames dans la structure
                                if (strcmp(nickk,clients.client[j].nick)==0) {
                                    indice_client=j;
                                    printf("%d\n",indice_client);
                                    break;
                                }
                            }
                            memset(message,'\0',80);
                            strcat(message,clients.client[indice_client].nick);
                            strcat(message," is connected with ip ");
                            strcat(message,clients.client[indice_client].ip);
                            strcat(message," at ");
                            strcat(message,clients.client[indice_client].date);
                            do_write(message,poll_set[i].fd);
                            memset(message,'\0',80);
                        }
                        if(strcmp(message,"quit\n")==0){

                            printf("le client a quitté\n");
                            close(poll_set[i].fd);
                            // quand un utilisateur quitte il faud décaler la liste des polls
                            for(j = i; j < nfds; j++){
                                poll_set[j].fd = poll_set[j+1].fd;
                            }
                            poll_set[i].fd=-1;
                            nfds--;

                        }
                        // cas d'une commande /who
                        else if(strcmp(message,"/who\n")==0){
                            memset(message,'\0',80);
                            strcat(message,"connected users are \n");
                            for( j = 0; j < MAX_SIZE; j++){
                                if(clients.client[j].donenick==1){
                                    strcat(message,clients.client[j].nick);
                                    strcat(message,"\n");
                                }
                            }
                            do_write(message,poll_set[i].fd);
                            memset(message,'\0',80);
                            
                        }
                        // ci dessous le cas d'un envoi de message normal
                        else{
                            if(c==0){
                                if(clients.client[i].donenick!=1){
                                    memset(message,'\0',80);
                                    strcat(message,"please enter a valid nickname to send a message");
                                    do_write(message,poll_set[i].fd);
                                    which_command(i,message,clients);   
                                }
                                else{
                                    do_write(message,poll_set[i].fd);
                                    memset(message,'\0',80);
                                }
                            }
                        }
                }
            }
        }
    }
    }
    //clean up server socket

    return 0;
}
